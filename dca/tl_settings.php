<?php

$GLOBALS['TL_DCA']['tl_settings']['palettes']['default'] = str_replace('maintenanceMode;', 'maintenanceMode;{google_legend},googleApiKey;', $GLOBALS['TL_DCA']['tl_settings']['palettes']['default']);


$GLOBALS['TL_DCA']['tl_settings']['fields']['googleApiKey'] = array
(
    'label'                   => &$GLOBALS['TL_LANG']['tl_settings']['googleApiKey'],
    'inputType'               => 'text',
    'eval'                    => array('tl_class'=>'w50')
);