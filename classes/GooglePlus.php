<?php

namespace Contao;

class GooglePlus extends \Frontend
{

    public $strTemplatePlus = "googlePlus_author";
    public $strGooglePlusWidgetUrl = "https://apis.google.com/js/plusone.js";
    public $strGooglePlusProfileUrl = "https://plus.google.com/u/0/";

    public function  __construct(){

    }

    /**
     * This Functions returns a google+ snippet
     * @param $objTemplate
     * @param $arr
     * @param $ele
     * @return mixed
     */
    public function GetProfile($objTemplate, $arr, $ele){
        try {

            if(!empty($arr)){
                $user = \UserModel::findBy('id',$arr['author']);
                if($user!=null){

                    $objTemplate->authorEmail = $user->email;

                    if($user->google != null){
                        $googlePlusTemp = new \FrontendTemplate($this->strTemplatePlus);
                        $googlePlusTemp->name  = $user->name;
                        $googlePlusTemp->email = $user->email;
                        if($user->googleImage != null){
                            $googlePlusTemp->url   = $user->googleProfileUrl;
                            $googlePlusTemp->img   = $user->googleImage;
                        } else {
                            if($GLOBALS['TL_CONFIG']['googleApiKey'] != null){
                                $googleData = $this->getGoogleProfileDate($user->google);
                                if($googleData['status'] == 200){
                                    $user->googleProfileUrl = $googleData['data']->url;
                                    $user->googleImage = $googleData['data']->image->url;
                                    $user->save();
                                }
                            } else {
                                $objTemplate->googleplus = $this->getGooglePlusWidgetFallback($user->google);
                            }
                        }
                        if($GLOBALS['TL_CONFIG']['googleApiKey'] != null){
                            $objRequest = new Request();
                            $objRequest->send('https://www.googleapis.com/plus/v1/people/'.$user->google.'?fields=image%2Curl&key='.$GLOBALS['TL_CONFIG']['googleApiKey']);
                            if (!$objRequest->hasError()){
                                $response = json_decode($objRequest->response);
                                $googlePlusTemp->name = $user->name;
                                $googlePlusTemp->email = $user->email;
                                $googlePlusTemp->url = $response->url;
                                $googlePlusTemp->img = $response->image->url."0";
                            }
                        }
                        $objTemplate->googleplus = $googlePlusTemp->parse();
                    } else {
                        $objTemplate->googleplus = $this->getGooglePlusWidgetFallback($user->google);
                    }
                }
                return $objTemplate;
            }
        } catch(\Exception $exception){
            $this->log($exception->getMessage(), __METHOD__, TL_ERROR);
        }
    }

    public function postLogin(User $objUser){
        try {
            if($objUser!=null){
                $user = \UserModel::findBy('id',$objUser->id);
                if($user!=null){
                    if($user->google != null){
                        $googleData = $this->getGoogleProfileDate($user->google);
                        if($googleData['status'] == 200){
                            if($googleData['data']->image->url != $user->googleImage){
                                $user->googleProfileUrl = $googleData['data']->url;
                                $user->googleImage = $googleData['data']->image->url."0";
                                $user->save();
                            }
                        }
                    }
                }
            }
        } catch(Exeption $exception){
            $this->log($exception->getMessage(), __METHOD__, TL_ERROR);
        }
        return $objUser;
    }

    protected function getGoogleProfileDate($userID=null){
        $response = array('status'=>400);
        try {
            if($GLOBALS['TL_CONFIG']['googleApiKey'] != null){
                $objRequest = new Request();
                $objRequest->send('https://www.googleapis.com/plus/v1/people/'.$userID.'?fields=image%2Curl&key='.$GLOBALS['TL_CONFIG']['googleApiKey']);
                if (!$objRequest->hasError()){
                    $response['status']= 200;
                    $response['data']  = json_decode($objRequest->response);
                } else {
                    $response['status']= 400;
                }
            } else {
                $response['status'] = 412;
                $response['msg']    = "Google API Key was not found - please define in settings";
            }
        } catch(\Exception $exception){
            $response['status'] = 500;
        }
        return $response;
    }

    protected function getGooglePlusWidgetFallback($googleID=null){
        $fallback = "";
        try {
            if(!in_array($this->strGooglePlusWidgetUrl,$GLOBALS['TL_JAVASCRIPT'])){
                $GLOBALS['TL_JAVASCRIPT'][] = $this->strGooglePlusWidgetUrl;
            }
            $fallback = '<div class="author"><g:plus href="https://plus.google.com/'.$googleID.'" rel="author"></g:plus></div>';
        } catch(\Exception $exception){
            $this->log($exception->getMessage(), __METHOD__, TL_ERROR);
        }
        return $fallback;
    }

    public function replaceTagInsertTags($strTag, $blnCache){
        global $objPage;
        if($strTag==null){return;}
        $arrSplit = explode('::', $strTag);

        if (isset($arrSplit[1])){
            switch($arrSplit[0]){
                case "googlePlusAuthorProfileUrl":
                    $user = \UserModel::findBy('id',$arrSplit[1]);
                    if($user != null){
                        if($user->googleProfileUrl !== null AND $user->googleProfileUrl !== ""){
                            return $user->googleProfileUrl;
                        } elseif($user->google !== null){
                            return $this->strGooglePlusProfileUrl.$user->google;
                        } else {
                            return false;
                        }
                    }
                    break;
                case "googlePlusAuthorImagePath":
                    $user = \UserModel::findBy('id',$arrSplit[1]);
                    if($user != null){
                        if($user->googleImage !== null){
                            return $user->googleImage;
                        } else {
                            return false;
                        }
                    }
                    break;
                case "googlePlusAuthorImage":
                    $user = \UserModel::findBy('id',$arrSplit[1]);
                    if($user != null){
                        if($user->googleImage !== null AND $user->googleImage !== ""){
                            return '<img src="'.$user->googleImage.'" alt="'.$user->name.'" class="img-responsive" />';
                        } else {
                            return $this->getGooglePlusWidgetFallback($user->google);
                        }
                    }
                    break;
                default:
                    break;
            }
        } else {

        }
        return false;
    }

}