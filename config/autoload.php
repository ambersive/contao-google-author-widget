<?php

/**
 * Register the classes
 */
ClassLoader::addClasses(array
(
    // Classes
    'Contao\GooglePlus'                    => 'system/modules/ambersive-googleprofile/classes/GooglePlus.php',
));

/**
 * Register the templates
 */
TemplateLoader::addFiles(array
(
    'googlePlus_author'               => 'system/modules/ambersive-googleprofile/templates/',
));