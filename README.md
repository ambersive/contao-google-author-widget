# Google+ Profil Widget for Contao 3.x+ #

by Ambersive.com

Provides a Google+ Widget
Just insert the following code snippet into the news template

```
#!php
<?php echo $this->googleplus; ?>

```

### Insert tags ###

#### Image path ####

```
{{googlePlusAuthorImagePath::IDofUser}}

```

#### Google+ Widget ####

```
{{googlePlusAuthorImage::IDofUser}}

```

#### Google+ Profile Url ####

```
{{googlePlusAuthorProfileUrl::IDofUser}}

```

### Versions ###

2014-05-11: Initial Commit
2014-06-07: Added insert tags
2014-06-18: Added insert tag (googlePlusAuthorProfileUrl) + Bugfixes