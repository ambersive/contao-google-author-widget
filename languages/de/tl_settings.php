<?php

/**
 * Legends
 */
$GLOBALS['TL_LANG']['tl_settings']['google_legend'] = "Google API";

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_settings']['googleApiKey'] = array('Google API-Key für Google+','Geben Sie den Google API-Key für Google+ hier ein.');