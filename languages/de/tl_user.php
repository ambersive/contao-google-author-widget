<?php

$GLOBALS['TL_LANG']['tl_user']['googleImage'] = array('Google Profilbild','Geben Sie den Pfad zum Profilbild ein.');
$GLOBALS['TL_LANG']['tl_user']['googleProfileUrl'] = array('Google Profil','Geben Sie die Url des Google+ Profils ein. (Vorsicht: Dieser Wert kann autoamtisch ersetzt werden)');